# Everplaces - Wordpress plugin

## Description
Wordpress plugin allowing you to display the content of your Everplaces collections (as a map or as an album).

## Getting started
### Installation
1. Download the plugin here: http://everplaces.github.io/everplaces-wordpress-plugin/Everplaces-for-WordPress.zip
2. Go to your WordPress admin
3. In the left sidebar first press Plugins and then Add New
4. In the top of that page find and click the button saying Upload
5. Use the installer to upload the zip file downloaded in step 1.

After that make sure to activate the plugin

### Use
1. First you need the ID or ID's of the collections you want to show locations from. The ID is in the url of your collection like so: `https://everplaces.com/my_username/collections/collection_name-COLLECTIONID.`
2. Copy and paste this into your blogpost: `[everplaces_plugin ids="COLLECTIONID" type="TTT" width="XXX" height="YYY"]`. 
3. Replace `COLLECTIONID` with the ID you have previously found. Replace `TTT` with `map` or `album` depending on how you want your locations displayed. Replace `XXX` and `YYY` with the width and the height of the map (in pixels). If album has been selected, width and height will be ignored and is therefore not needed.

### Note
You can set several collection id in the `ids` attribute to display several collections in one map/album. Just separate them with a comma.

## Examples
`[everplaces_plugin ids="IO7E3jEzMDc" type="album"]`

![Album with one collection](http://i.imgur.com/cPwYIBq.png "Album with one collection")

`[everplaces_plugin ids="IO7E3jEzMDc" type="map" width="600" height="600"]`

![Map with one collection](http://i.imgur.com/2yCTDpNl.png "Map with one collection")

`[everplaces_plugin ids="IO7E3jEzMDc, J2GpsjEzMzM" type="map" width="600" height="600"]`

![Map with two collections](http://i.imgur.com/yPO9rKL.png "Map with two collections")
