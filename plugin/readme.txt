=== Everplaces for WordPress ===
Contributors: Everplaces
Tags: map, places, locations, geolocation, travel, travelmap
Requires at least: 3.4
Tested up to: 3.6
Stable tag: 0.9
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

The best way to show your recommendations in Everplaces on your blog.
Makes it easy to add beautiful maps and albums of your recommendations to your next blog post.

== Description ==

Are you writing a travelblog or creating a list of places to visit in your city, then just add all your recommendations to a collections in [Everplaces](https://everplaces.com/), and add them to your blogpost with this plugin.

You can display your locations on both a map and as and album into any blog post.

== Installation ==

1. Upload `everplaces-for-wordpress.php` to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress

**Use in blogpost**

1. First you need the ID or ID's of the collections you want to show locations from. The ID is in the url of your collection like so: `https://everplaces.com/my_username/collections/collection_name-COLLECTIONID.`
1. Copy and paste this into your blogpost: `[everplaces_plugin ids="COLLECTIONID" type="TTT" width="XXX" height="YYY"]`. 
1. Replace `COLLECTIONID` with the ID you have previously found. Replace `TTT` with `map` or `album` depending on how you want your locations displayed. Replace `XXX` and `YYY` with the width and the height of the map (in pixels). If album has been selected, width and height will be ignored and is therefore not needed.

**Example**

Multiple ID's: `[everplaces_plugin ids="82kkecd99d8, fdd-kd782jn" type="map" width="640" height="360"]`