<?php
/*
Plugin Name: Everplaces for WordPress
Plugin URI: 
Description: The best way to show your recommendations in Everplaces on your blog. Makes it easy to add beautiful maps and albums of your recommendations to your next blog post.
Version: 0.9
Author: Everplaces
Author URI: https://everplaces.com
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
*/

define ( "DEFAULT_HEIGHT", "400px" );
define ( "DEFAULT_WIDTH",  "100%" );

// callback raised everytime the [everplaces_plugin ...] appears.
function everplaces_plugin_handler ( $atts ) {
    // create the plugin container
    $widget = '<div data-evp-id="' . $atts['ids'] . '" data-evp-type="' . $atts['type'] . '"';

    if ( $atts['limit'] ) {
        $widget .= ' data-evp-limit="' . $atts['limit'] . '" ';
    }

    // set height and width if the plugin is a map
    if ( $atts['type'] === 'map' ) {
        
        $height = $atts['height'];
        $width  = $atts['width'];
    
        if ( ! $height )  $height = DEFAULT_HEIGHT;
        if ( ! $width )   $width  = DEFAULT_WIDTH;

        $widget .= ' style="height:' . $height . ';width:' . $width. '"';
    }

    $widget .= '></div>';

    // add the javascript to the page (css is direclty loaded from the widget)
    wp_register_script  ( 'everplaces_widget', 'http://widget.everplaces.com/javascripts/app.js' );
    wp_enqueue_script   ( 'everplaces_widget' );

    return $widget;
}

add_shortcode ( 'everplaces_plugin', 'everplaces_plugin_handler' );

?>